package com.epam.cacheservice;

import com.epam.cacheservice.models.ProductWithCreateTime;
import com.epam.cacheservice.validators.ProductValidator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;

@EnableEurekaClient
@SpringBootApplication
public class CacheServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CacheServiceApplication.class, args);
    }

    @Bean
    Map<Long, ProductWithCreateTime> cachedProductsMap(){
        return new HashMap<>();
    }

    @Bean
    public Validator productValidator() {
        return new ProductValidator();
    }

}
