package com.epam.cacheservice.models;

public class ProductGroup {

    private Long id;
    private String groupName;
    private String price;
    private String created;

    public ProductGroup() {

    }

    public ProductGroup(String id) {
        this.id = Long.parseLong(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String toString() {
        return getGroupName();
    }
}
