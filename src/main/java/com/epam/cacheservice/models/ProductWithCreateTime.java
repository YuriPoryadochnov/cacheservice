package com.epam.cacheservice.models;

import java.sql.Timestamp;

public class ProductWithCreateTime {
    private final Product product;
    private Long timestamp;

    public ProductWithCreateTime(Product product) {
        this.product = product;
        this.timestamp = getCurrentTimestamp().getTime();
    }

    public Product getProduct() {
        return product;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = getCurrentTimestamp().getTime();
    }

    private Timestamp getCurrentTimestamp(){
        return new Timestamp(System.currentTimeMillis());
    }
}
