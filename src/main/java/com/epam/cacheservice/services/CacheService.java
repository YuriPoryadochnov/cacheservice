package com.epam.cacheservice.services;

import com.epam.cacheservice.models.Product;
import com.epam.cacheservice.models.ProductWithCreateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Map;

@Service
public class CacheService {

    @Value("${product.wait.timeInMs}")
    private long PRODUCT_WAIT_TIME;

    @Autowired
    private Map<Long, ProductWithCreateTime> cachedProductsMap;


    public Product view(long id) {
        ProductWithCreateTime productWithCreateTime = cachedProductsMap.get(id);
        if (productWithCreateTime != null) {
            Long productTime = productWithCreateTime.getTimestamp();
            Long currentTime = new Timestamp(System.currentTimeMillis()).getTime();
            if (currentTime - productTime <= PRODUCT_WAIT_TIME) return productWithCreateTime.getProduct();
        }
        return null;
    }

    public Product create(Product product) {
        ProductWithCreateTime productWithCreateTime = new ProductWithCreateTime(product);
        long productId = product.getId();
        if (cachedProductsMap.get(productId) == null) {
            cachedProductsMap.put(productId, productWithCreateTime);
            logInConsole("Cache: Added new product " + product.getName() + " to map.");
        } else {
            cachedProductsMap.replace(productId, productWithCreateTime);
            logInConsole("Cache: Updated data product " + product.getName() + ".");
        }
        return product;
    }

    private void logInConsole(String message) {
        System.out.println(message);
    }
}

