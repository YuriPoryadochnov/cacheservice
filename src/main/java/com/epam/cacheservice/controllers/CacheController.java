package com.epam.cacheservice.controllers;

import com.epam.cacheservice.models.Product;
import com.epam.cacheservice.services.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    Validator productValidator;

    @Autowired
    CacheService cacheService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(productValidator);
    }

    @GetMapping("/product/{id}")
    public Product view(@PathVariable("id") long id) {
        return cacheService.view(id);
    }

    @PostMapping("/product")
    public Product create(@RequestBody @Valid Product product) {
        return cacheService.create(product);
    }

}
